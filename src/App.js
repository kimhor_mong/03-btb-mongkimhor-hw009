import NavMenu from './components/NavMenu';
import 'bootstrap/dist/css/bootstrap.css';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Home from './pages/Home';
import Video from './pages/Video';
import Account from './pages/Account';
import Welcome from './pages/Welcome';
import Auth from './pages/Auth';
import { Container } from 'react-bootstrap';
import ArticleDetail from './components/ArticleDetail';
import ProtectedRoute from '../src/components/ProtectedRoute';

function App() {
	return (
		<Router>
			<NavMenu />
			<Container>
				<Switch>
					<Route exact path='/' component={Home} />
					<Route path='/detail/:id' component={ArticleDetail} />
					<Route path='/video' component={Video} />
					<Route path='/account' component={Account} />
					<ProtectedRoute path='/welcome'>
						<Welcome />
					</ProtectedRoute>

					<Route path='/auth' component={Auth} />
				</Switch>
			</Container>
		</Router>
	);
}

export default App;
