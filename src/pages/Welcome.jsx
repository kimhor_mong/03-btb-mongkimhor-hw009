import { Button } from 'react-bootstrap';
import React from 'react';
import { useHistory } from 'react-router';
import auth from '../auth/auth';

function Welcome() {
	let history = useHistory();
	return (
		<div>
			<h1>Welcome</h1>
			<Button
				onClick={() => {
					auth.logout(() => {
						history.push('/auth');
					});
				}}
			>
				Logout
			</Button>
		</div>
	);
}

export default Welcome;
