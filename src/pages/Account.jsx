import React from 'react';
import { Link, Route, useRouteMatch } from 'react-router-dom';
import AccountDetail from '../components/AccountDetail';

function Account() {
	let { path, url } = useRouteMatch();
	return (
		<div>
			<h1>Account</h1>
			<ul>
				<li>
					<Link to={`${url}/netflix`}>Netflix</Link>
				</li>
				<li>
					<Link to={`${url}/zillow-group`}>Zillow Group</Link>
				</li>
				<li>
					<Link to={`${url}/yahoo`}>Yahoo</Link>
				</li>
				<li>
					<Link to={`${url}/modus-create`}>Modus Create</Link>
				</li>
			</ul>
			<Route path={`${path}/:account`} component={AccountDetail} />
		</div>
	);
}

export default Account;
