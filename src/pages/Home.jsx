import React, { useState } from 'react';
import { Card, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';
function Home() {
	const [articles, setArticles] = useState([
		{
			id: 1,
			title: 'Card Title',
			content: 'content',
			image: 'https://media.istockphoto.com/photos/colored-powder-explosion-on-black-background-picture-id1057506940?k=6&m=1057506940&s=612x612&w=0&h=C11yA-ESqeuCX63QkRpPyWmAMXJJvZw0niQluGnATlI=',
		},
		{
			id: 2,
			title: 'Card Title',
			content: 'content',
			image: 'https://filedn.com/ltOdFv1aqz1YIFhf4gTY8D7/ingus-info/BLOGS/Photography-stocks3/stock-photography-slider.jpg',
		},
		{
			id: 3,
			title: 'Card Title',
			content: 'content',
			image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRwGPF8X5lgcLBtUZUXV9kPPpfw7IuIsTq3uQ&usqp=CAU',
		},
		{
			id: 4,
			title: 'Card Title',
			content: 'content',
			image: 'https://cdn.pixabay.com/photo/2016/01/08/11/57/butterflies-1127666__340.jpg',
		},
	]);
	return (
		<Row className='mt-3'>
			{articles.map((article) => (
				<Col key={article.id} md={4} lg={3} className='mt-2'>
					<Card>
						<Card.Img variant='top' src={article.image} width='50%' height='200px' />
						<Card.Body>
							<Card.Title>{article.title}</Card.Title>
							<Card.Text>{article.content}</Card.Text>
							<Link className='btn btn-light' to={`/detail/${article.id}`}>
								Read More
							</Link>
						</Card.Body>
					</Card>
				</Col>
			))}
		</Row>
	);
}

export default Home;
