import React from 'react';
import { ButtonGroup, Button } from 'react-bootstrap';
import {Route, NavLink, useRouteMatch } from 'react-router-dom';
import AnimationCategory from '../components/AnimationCategory';
import MovieCategory from '../components/MovieCategory';

function Video() {
	let { path, url } = useRouteMatch();

	return (
		<div>
			<h1>Video</h1>
			<ButtonGroup aria-label='Basic example'>
				<Button as={NavLink} to={`${url}/movie`} variant='secondary'>
					Movie
				</Button>
				<Button as={NavLink} to={`${url}/animation`} variant='secondary'>
					Animation
				</Button>
			</ButtonGroup>

			<Route path={`${path}/movie`} component={MovieCategory} />
			<Route path={`${path}/animation`} component={AnimationCategory} />
		</div>
	);
}

export default Video;
