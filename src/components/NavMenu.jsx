import React from 'react';
import { Navbar, Container, Nav, Form, FormControl, Button } from 'react-bootstrap';
import { NavLink } from 'react-router-dom';

function NavMenu() {
	return (
		<Navbar bg='light' expand='lg'>
			<Container>
				<Navbar.Brand as={NavLink} to='/'>
					React Router
				</Navbar.Brand>
				<Navbar.Toggle aria-controls='basic-navbar-nav' />
				<Navbar.Collapse id='basic-navbar-nav'>
					<Nav className='me-auto'>
						<Nav.Link as={NavLink} to='/'>
							Home
						</Nav.Link>
						<Nav.Link as={NavLink} to='/video'>
							Video
						</Nav.Link>
						<Nav.Link as={NavLink} to='/account'>
							Account
						</Nav.Link>
						<Nav.Link as={NavLink} to='/welcome'>
							Welcome
						</Nav.Link>
						<Nav.Link as={NavLink} to='/auth'>
							Auth
						</Nav.Link>
					</Nav>

					<Form className='d-flex'>
						<FormControl type='search' placeholder='Search' className='me-2' aria-label='Search' />
						<Button variant='outline-success'>Search</Button>
					</Form>
				</Navbar.Collapse>
			</Container>
		</Navbar>
	);
}

export default NavMenu;
