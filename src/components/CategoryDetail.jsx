import React from 'react';
import useQuery from '../custom/useQuery';

function CategoryDetail() {
	let query = useQuery();
	return (
		<div>
			<h3>
				Please Choose Category: <span style={{ color: 'red' }}>{query.get('type')}</span>{' '}
			</h3>
		</div>
	);
}

export default CategoryDetail;
