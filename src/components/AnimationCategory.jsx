import React from 'react';
import { ButtonGroup, Button } from 'react-bootstrap';
import { Route, NavLink, useRouteMatch } from 'react-router-dom';
import CategoryDetail from './CategoryDetail';

function AnimationCategory() {
	let { path, url } = useRouteMatch();
	return (
		<div>
			<h1>Animation Category</h1>

			<ButtonGroup aria-label='Basic example'>
				<Button as={NavLink} to={`${url}?type=Action`} variant='secondary'>
					Action
				</Button>
				<Button as={NavLink} to={`${url}?type=Romance`} variant='secondary'>
					Romance
				</Button>
				<Button as={NavLink} to={`${url}?type=Comedy`} variant='secondary'>
					Comedy
				</Button>
			</ButtonGroup>

			<Route path={`${path}`} component={CategoryDetail} />
		</div>
	);
}

export default AnimationCategory;
