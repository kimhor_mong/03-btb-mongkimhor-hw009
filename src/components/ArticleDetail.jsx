import React from 'react';

import { useParams } from 'react-router-dom';

function ArticleDetail() {
	let { id } = useParams();
	return <p>Detail {id}</p>;
}

export default ArticleDetail;
