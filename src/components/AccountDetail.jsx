import React from 'react';
import { useParams } from 'react-router-dom';

function AccountDetail() {
	let { account } = useParams();

	return <h4>ID: {account}</h4>;
}

export default AccountDetail;
